//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

	{"| ", "check_pac.sh",					0,  		12},
	{"", "networkstats",					1,  		0},
	{"", "volume.sh",					0,		10},
	{"", "backlight.sh",					0,		11},
	{"", "cputemp.sh",					5,		0},
	{"", "battery.sh",					5,		0},
	{"", "internet_status.sh",				5,		0},
	{"", "weather",						1800,		0},
	{" ", "date '+%a %b %d %R |'",				5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 4;
